(ns app.heroes.heroes
  (:require [app.heroes.heroes_repo :as repo]))

(defn get-hero [{{:keys [hero]} :path-params}]

  (let [superman (repo/find-hero (keyword hero))]
    (cond (map? superman)
          {:status 200 :body superman}
          :else {:status 400 :body nil})))

(defn get-heroes [_] {:status 200 :body (repo/find-heroes)})
