(ns app.heroes.heroes_repo
  (:require [app.db.db :as db]
            [monger.collection :as mc]))

(def superheroes-coll "superheroes")

(defn find-heroes [] (let [conn (db/get-conn)]
                       (mc/find-maps conn superheroes-coll)))

(defn find-hero [nickname] (let [conn (db/get-conn)]
                             (mc/find-one-as-map conn superheroes-coll
                                                 {:nickname nickname})))