;; small helper for reading JWKS data from Auth0 and converting it to a java.security.PublicKey

(ns app.jwt.jwks (:require
                  [cheshire.core :as json]
                  [buddy.core.keys :as keys]))

(defn- fetch-jwks [jwks-url] (json/parse-string (slurp jwks-url) true))

;; memorize jwks to avoid http call in every jwt verification
(def fetch-jwks-memo (memoize fetch-jwks))

(defn- jwks->pubkey [jwks] (->> jwks
                                (:keys)
                                (first)
                                (keys/jwk->public-key)))

(defn jwks-url->pubkey [jwks-url]
  (jwks->pubkey (fetch-jwks-memo jwks-url)))
