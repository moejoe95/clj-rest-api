  (ns app.jwt.jwt (:require [buddy.sign.jwt :as jwt]
                            [io.pedestal.interceptor.helpers :as interceptor]
                            [app.jwt.jwks :as jwks]))


(defn- unauthorized [text]
  {:status  401
   :headers {}
   :body    text})

(defn- verify [jwt, jwks-url]
  (jwt/unsign jwt (jwks/jwks-url->pubkey jwks-url) {:alg :rs256}))

(defn decode-jwt [{:keys [jwk-endpoint]}]
  (interceptor/before
   ::decode-jwt
   (fn [ctx] (if-let [auth-header (get-in ctx [:request :headers "authorization"])]
               (try
                 (verify auth-header jwk-endpoint)
                 (catch Exception _
                   (assoc ctx :response (unauthorized "JWT not valid"))))
               ((assoc ctx :response (unauthorized "JWT not provided"))
                (assoc-in ctx [:request :claims] {}))))))
