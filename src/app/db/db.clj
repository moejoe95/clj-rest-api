(ns app.db.db
  (:require [monger.core :as mg]))

(def db-name "clj-api-database")

(defn get-conn [] (let [conn (mg/connect)]
                    (mg/get-db conn db-name)))

