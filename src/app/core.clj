(ns app.core (:require [io.pedestal.http :as http]
                       [environ.core :refer [env]]
                       [app.heroes.heroes :as heroes]
                       [app.jwt.jwt :refer [decode-jwt]]))


(def routes #{["/heroes/:hero" :get heroes/get-hero :route-name :get-hero]
              ["/heroes" :get heroes/get-heroes :route-name :get-heroes]})

(def jwk-endpoint "https://dev-dogh3506.eu.auth0.com/.well-known/jwks.json")

(def service-map (-> {::http/routes routes
                      ::http/type   :immutant
                      ::http/host   "0.0.0.0"
                      ::http/join?  false
                      ::http/port   (Integer. (or (env :port) 5000))}
                     http/default-interceptors
                     (update ::http/interceptors into [http/json-body
                                                       (decode-jwt {:jwk-endpoint jwk-endpoint})])))

;; start web server
(defn start []
  (http/start (http/create-server service-map)))


;; for interactive development in REPL
(defonce server (atom nil))

(defn start-dev []
  (reset! server
          (http/start (http/create-server
                       (assoc service-map
                              ::http/join? false)))))

(defn stop-dev []
  (http/stop @server))

(defn restart []
  (stop-dev)
  (start-dev))


(comment

  (start-dev)

  (restart)

  (stop-dev))